# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR FusionDirectory Project
# This file is distributed under the same license as the FusionDirectory package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: FusionDirectory VERSION\n"
"Report-Msgid-Bugs-To: bugs@fusiondirectory.org\n"
"POT-Creation-Date: 2018-02-07 11:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: fusiondirectory <contact@fusiondirectory.org>, 2017\n"
"Language-Team: Greek (Greece) (https://www.transifex.com/fusiondirectory/teams/12202/el_GR/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: el_GR\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin/groups/posix/class_posixGroup.inc:34
msgid "Group"
msgstr "Ομάδα"

#: admin/groups/posix/class_posixGroup.inc:35
msgid "POSIX group information"
msgstr "Πληροφορίες ομάδας POSIX"

#: admin/groups/posix/class_posixGroup.inc:38
msgid "POSIX group"
msgstr "Ομάδα POSIX"

#: admin/groups/posix/class_posixGroup.inc:39
msgid "POSIX user group"
msgstr "Ομάδα χρηστών POSIX"

#: admin/groups/posix/class_posixGroup.inc:57
msgid "Properties"
msgstr "Ιδιότητες"

#: admin/groups/posix/class_posixGroup.inc:61
msgid "Name"
msgstr "Όνομα"

#: admin/groups/posix/class_posixGroup.inc:61
msgid "Name of this group"
msgstr "Όνομα αυτής της ομάδας"

#: admin/groups/posix/class_posixGroup.inc:66
msgid "Description"
msgstr "Περιγραφή"

#: admin/groups/posix/class_posixGroup.inc:66
msgid "Short description of this group"
msgstr "Σύντομη περιγραφή αυτής της ομάδας"

#: admin/groups/posix/class_posixGroup.inc:70
msgid "Force GID"
msgstr "Εξαναγκασμός GID"

#: admin/groups/posix/class_posixGroup.inc:70
msgid "Force GID value for this group"
msgstr "Εξαναγκασμός τιμής GID για αυτή την ομάδα"

#: admin/groups/posix/class_posixGroup.inc:74
#: admin/groups/posix/class_posixGroup.inc:150
#: personal/posix/class_posixAccount.inc:425
msgid "GID"
msgstr "GID"

#: admin/groups/posix/class_posixGroup.inc:74
msgid "GID value for this group"
msgstr "Τιμή GID για αυτήν την ομάδα"

#: admin/groups/posix/class_posixGroup.inc:81
#: admin/groups/posix/class_posixGroup.inc:84
msgid "Group members"
msgstr "Μέλη ομάδας"

#: admin/groups/posix/class_posixGroup.inc:91
#: personal/posix/class_posixAccount.inc:203
msgid "System trust"
msgstr "Εμπιστοσύνη συστήματος"

#: admin/groups/posix/class_posixGroup.inc:95
#: personal/posix/class_posixAccount.inc:207
msgid "Trust mode"
msgstr "Λειτουργία επιστοσύνης"

#: admin/groups/posix/class_posixGroup.inc:95
#: personal/posix/class_posixAccount.inc:207
msgid "Type of authorization for those hosts"
msgstr "Τύπος εξουσιοδότησή για αυτά τα συστήματα"

#: admin/groups/posix/class_posixGroup.inc:99
#: personal/posix/class_posixAccount.inc:211
#: personal/posix/class_posixAccount.inc:233
msgid "disabled"
msgstr "απενεργοποιημένο"

#: admin/groups/posix/class_posixGroup.inc:99
#: personal/posix/class_posixAccount.inc:211
#: personal/posix/class_posixAccount.inc:233
msgid "full access"
msgstr "Πλήρης Πρόσβαση"

#: admin/groups/posix/class_posixGroup.inc:99
#: personal/posix/class_posixAccount.inc:211
msgid "allow access to these hosts"
msgstr "να επιτραπεί πρόσβαση σε αυτά τα host"

#: admin/groups/posix/class_posixGroup.inc:102
msgid "Only allow this group to connect to this list of hosts"
msgstr ""
"Επιτρέπεται σ'αυτή την ομάδα να συνδέεται στην ακόλουθη λίστα διακομιστών"

#: config/posix/class_posixConfig.inc:28
msgid "POSIX configuration"
msgstr ""

#: config/posix/class_posixConfig.inc:29
msgid "FusionDirectory POSIX plugin configuration"
msgstr ""

#: config/posix/class_posixConfig.inc:42
msgid "Posix"
msgstr "Posix"

#: config/posix/class_posixConfig.inc:46
msgid "POSIX groups RDN"
msgstr ""

#: config/posix/class_posixConfig.inc:46
msgid "The branch where POSIX groups are stored."
msgstr ""

#: config/posix/class_posixConfig.inc:51
msgid "Group/user min id"
msgstr ""

#: config/posix/class_posixConfig.inc:52
msgid ""
"The minimum assignable user or group id to avoid security leaks with id 0 "
"accounts."
msgstr ""

#: config/posix/class_posixConfig.inc:57
msgid "Next id hook"
msgstr ""

#: config/posix/class_posixConfig.inc:57
msgid ""
"A script to be called for finding the next free id number for users or "
"groups."
msgstr ""
"Ένα σενάριο που θα κληθεί για την εύρεση του επόμενου ελεύθερου id για "
"χρήστες ή ομάδες."

#: config/posix/class_posixConfig.inc:61
msgid "Base number for user id"
msgstr "Αριθμός βάση για user id"

#: config/posix/class_posixConfig.inc:62
msgid "Where to start looking for a new free user id."
msgstr "Τοποθεσία εύρεσης νέου ελεύθερου user id."

#: config/posix/class_posixConfig.inc:67
msgid "Base number for group id"
msgstr "Αριθμός βάση για group id"

#: config/posix/class_posixConfig.inc:68
msgid "Where to start looking for a new free group id."
msgstr "Τοποθεσία εύρεσης νέου ελεύθερου group id."

#: config/posix/class_posixConfig.inc:73
msgid "Id allocation method"
msgstr "Μέθοδος διανομής id"

#: config/posix/class_posixConfig.inc:73
msgid "Method to allocate user/group ids"
msgstr "Μέθοδος για την διανομή user/group id"

#: config/posix/class_posixConfig.inc:76
msgid "Traditional"
msgstr "Παραδοσιακή"

#: config/posix/class_posixConfig.inc:76
msgid "Samba unix id pool"
msgstr ""

#: config/posix/class_posixConfig.inc:79
msgid "Pool user id min"
msgstr ""

#: config/posix/class_posixConfig.inc:79
msgid "Minimum value for user id when using pool method"
msgstr ""

#: config/posix/class_posixConfig.inc:84
msgid "Pool user id max"
msgstr ""

#: config/posix/class_posixConfig.inc:84
msgid "Maximum value for user id when using pool method"
msgstr ""

#: config/posix/class_posixConfig.inc:89
msgid "Pool group id min"
msgstr ""

#: config/posix/class_posixConfig.inc:89
msgid "Minimum value for group id when using pool method"
msgstr ""

#: config/posix/class_posixConfig.inc:94
msgid "Pool group id max"
msgstr ""

#: config/posix/class_posixConfig.inc:94
msgid "Maximum value for group id when using pool method"
msgstr ""

#: config/posix/class_posixConfig.inc:101
msgid "Shells"
msgstr ""

#: config/posix/class_posixConfig.inc:105
msgid "Available shells"
msgstr ""

#: config/posix/class_posixConfig.inc:105
msgid "Available POSIX shells for FD users."
msgstr ""

#: config/posix/class_posixConfig.inc:112
msgid "Default shell"
msgstr ""

#: config/posix/class_posixConfig.inc:112
msgid "Shell used by default when activating Unix tab."
msgstr ""

#: personal/posix/class_posixAccount.inc:88
#: personal/posix/class_posixAccount.inc:117
msgid "Unix"
msgstr "Unix"

#: personal/posix/class_posixAccount.inc:89
msgid "Edit users POSIX settings"
msgstr "Επεξεργασία ρυθμίσεων χρηστών POSIX"

#: personal/posix/class_posixAccount.inc:121
msgid "Home directory"
msgstr "Προσωπικός κατάλογος"

#: personal/posix/class_posixAccount.inc:121
msgid "The path to the home directory of this user"
msgstr "Η διαδρομή για τον αρχικό κατάλογο του χρήστη"

#: personal/posix/class_posixAccount.inc:126
msgid "Shell"
msgstr "Κέλυφος"

#: personal/posix/class_posixAccount.inc:126
msgid "Which shell should be used when this user log in"
msgstr "Ποιο κέλυφος πρέπει να χρησιμοποιηθεί όταν ο χρήστης συνδεθεί"

#: personal/posix/class_posixAccount.inc:128
msgid "unconfigured"
msgstr "μη ρυθμισμένο"

#: personal/posix/class_posixAccount.inc:132
msgid "Primary group"
msgstr "Κύρια ομάδα"

#: personal/posix/class_posixAccount.inc:132
msgid "Primary group for this user"
msgstr "Κύρια ομάδα του χρήστη"

#: personal/posix/class_posixAccount.inc:136
msgid "Status"
msgstr "Κατάσταση"

#: personal/posix/class_posixAccount.inc:136
msgid "Status of this user unix account"
msgstr "Κατάσταση του λογαριασμού unix του χρήστη"

#: personal/posix/class_posixAccount.inc:140
msgid "Force user/group id"
msgstr "Εξαναγκασμός user/group id"

#: personal/posix/class_posixAccount.inc:140
msgid "Force user id and group id values for this user"
msgstr ""
"Υποχρέωση συμπλήρωσης τιμών για user id και group id για αυτόν τον χρήστη"

#: personal/posix/class_posixAccount.inc:144
msgid "User id"
msgstr "User id"

#: personal/posix/class_posixAccount.inc:144
msgid "User id value for this user"
msgstr "Τιμή User id για αυτόν τον χρήστη."

#: personal/posix/class_posixAccount.inc:149
msgid "Group id"
msgstr "Group id"

#: personal/posix/class_posixAccount.inc:149
msgid "Group id value for this user"
msgstr "Τιμή Group id για αυτόν τον χρήστη"

#: personal/posix/class_posixAccount.inc:156
#: personal/posix/class_posixAccount.inc:159
msgid "Group membership"
msgstr "Μέλη ομάδας"

#: personal/posix/class_posixAccount.inc:163
msgid "Account"
msgstr "Λογαριασμός"

#: personal/posix/class_posixAccount.inc:167
msgid "User must change password on first login"
msgstr ""
"Ο χρήστης θα πρέπει να αλλάξει τον κωδικό πρόσβασης κατά την πρώτη του "
"σύνδεση"

#: personal/posix/class_posixAccount.inc:167
msgid ""
"User must change password on first login (needs a value for Delay before "
"forcing password change)"
msgstr ""
"Ο χρήστης πρέπει να αλλάξει συνθηματικό στην πρώτη σύνδεση (η τιμή στο πεδίο"
" Καθυστέρηση δηλώνει το πόσες φορές μπορεί ο χρήστης να συνδεθεί πριν "
"αναγκαστεί να αλλάξει συνθηματικό)"

#: personal/posix/class_posixAccount.inc:171
msgid "Minimum delay between password changes (days)"
msgstr ""

#: personal/posix/class_posixAccount.inc:171
msgid ""
"The user won't be able to change his password before this number of days "
"(leave empty to disable)"
msgstr ""

#: personal/posix/class_posixAccount.inc:176
msgid "Delay before forcing password change (days)"
msgstr "Καθυστέρηση πριν την υποχρεωτική αλλαγή κωδικού πρόσβασης (σε ημέρες)"

#: personal/posix/class_posixAccount.inc:176
msgid ""
"The user will be forced to change his password after this number of days "
"(leave empty to disable)"
msgstr ""
"Ο χρήστης θα υποχρεωθεί να αλλάξει τον κωδικό πρόσβασής του μετά από αυτό "
"τον αριθμό ημερών (αφήστε κενό για απενεργοποίηση)"

#: personal/posix/class_posixAccount.inc:181
msgid "Password expiration date"
msgstr "Ημερομηνία λήξης κωδικού πρόσβασης"

#: personal/posix/class_posixAccount.inc:181
msgid ""
"Date after which this user password will expire (leave empty to disable)"
msgstr ""
"Ημερομηνία μετά από την οποία θα λήγει ο κωδικός πρόσβασης (αφήστε κενό για "
"απενεργοποίηση)"

#: personal/posix/class_posixAccount.inc:186
msgid "Delay of inactivity before disabling user (days)"
msgstr "Ημέρες αδράνειας πριν από την απενεργοποίηση του χρήστη"

#: personal/posix/class_posixAccount.inc:186
msgid ""
"Maximum delay of inactivity after password expiration before the user is "
"disabled (leave empty to disable)"
msgstr ""
"Μέγιστος αριθμός ημερών αδράνειας μετά την λήξη του κωδικού πρόσβασης πριν "
"την απενεργοποίηση του χρήστη (αφήστε κενό για απενεργοποίηση)"

#: personal/posix/class_posixAccount.inc:191
msgid "Delay for user warning before password expiry (days)"
msgstr "Ημέρες μεταξύ προειδοποίησης χρήστη και λήξης κωδικού:"

#: personal/posix/class_posixAccount.inc:191
msgid ""
"The user will be warned this number of days before his password expiration "
"(leave empty to disable)"
msgstr ""
"Ο χρήστης θα προειδοποιείται αυτόν τον αριθμό των ημερών πριν από τη λήξη "
"του κωδικού πρόσβασής του (αφήστε κενό για απενεργοποίηση)"

#: personal/posix/class_posixAccount.inc:214
msgid "Only allow this user to connect to this list of hosts"
msgstr ""
"Επιτρέπεται σ'αυτόν τον χρήστη να συνδέεται στην ακόλουθη λίστα διακομιστών"

#: personal/posix/class_posixAccount.inc:292
msgid "automatic"
msgstr "αυτόματο"

#: personal/posix/class_posixAccount.inc:353
msgid "expired"
msgstr " έληξε"

#: personal/posix/class_posixAccount.inc:355
msgid "grace time active"
msgstr "χρονικό διάστημα χάριτος ενεργό"

#: personal/posix/class_posixAccount.inc:358
#: personal/posix/class_posixAccount.inc:360
#: personal/posix/class_posixAccount.inc:362
msgid "active"
msgstr "ενεργό"

#: personal/posix/class_posixAccount.inc:358
msgid "password expired"
msgstr "ο κωδικός πρόσβασης έχει λήξει"

#: personal/posix/class_posixAccount.inc:360
msgid "password not changeable"
msgstr "ο κωδικός πρόσβασης δεν μπορεί να αλλάξει"

#: personal/posix/class_posixAccount.inc:422
msgid "UID"
msgstr ""

#: personal/posix/class_posixAccount.inc:537
#, php-format
msgid "Group of user %s"
msgstr "Ομάδα του χρήστη %s"

#: personal/posix/class_posixAccount.inc:783
#: personal/posix/class_posixAccount.inc:804
#: personal/posix/class_posixAccount.inc:998
msgid "Warning"
msgstr "Προειδοποίηση"

#: personal/posix/class_posixAccount.inc:783
#, php-format
msgid "Unknown ID allocation method \"%s\"!"
msgstr ""

#: personal/posix/class_posixAccount.inc:805
#, php-format
msgid "Timeout while waiting for lock. Ignoring lock from %s!"
msgstr ""

#: personal/posix/class_posixAccount.inc:832
#: personal/posix/class_posixAccount.inc:872
#: personal/posix/class_posixAccount.inc:884
#: personal/posix/class_posixAccount.inc:888
#: personal/posix/class_posixAccount.inc:895
#: personal/posix/class_posixAccount.inc:904
#: personal/posix/class_posixAccount.inc:966
msgid "Error"
msgstr "Σφάλμα"

#: personal/posix/class_posixAccount.inc:832
#: personal/posix/class_posixAccount.inc:872
#: personal/posix/class_posixAccount.inc:884
#: personal/posix/class_posixAccount.inc:888
#: personal/posix/class_posixAccount.inc:895
#: personal/posix/class_posixAccount.inc:904
msgid "Cannot allocate a free ID:"
msgstr "Δεν είναι δυνατό να ανατεθεί μια free ID:"

#: personal/posix/class_posixAccount.inc:832
#, php-format
msgid "%sPoolMin >= %sPoolMax!"
msgstr ""

#: personal/posix/class_posixAccount.inc:864
msgid "LDAP error"
msgstr "Σφάλμα LDAP"

#: personal/posix/class_posixAccount.inc:872
msgid "sambaUnixIdPool is not unique!"
msgstr "sambaUnixIdPool  δεν είναι μοναδική!"

#: personal/posix/class_posixAccount.inc:884
#: personal/posix/class_posixAccount.inc:888
msgid "no ID available!"
msgstr "Μη διαθέσιμη ID!"

#: personal/posix/class_posixAccount.inc:904
msgid "maximum tries exceeded!"
msgstr "Υπέρβαση μεγίστου ορίου προσπαθειών!"

#: personal/posix/class_posixAccount.inc:966
msgid "Cannot allocate a free ID!"
msgstr "Δεν είναι δυνατή η διάθεση μιας  free ID!"

#: personal/posix/class_posixAccount.inc:998
msgid "\"nextIdHook\" is not available. Using default base!"
msgstr ""
